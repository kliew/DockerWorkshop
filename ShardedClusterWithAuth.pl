# Creates a specified number of shard, query, and config servers for MongoDB 3.0.3
# Increase $sleepTime or free up CPU if the cluster fails to start properly
# Mongo servers can be accessed by $hostP:$exposedPort
# You must manually enable sharding for each collection and database
# 	docs.mongodb.org/manual/tutorial/deploy-shard-cluster/#enable-sharding-for-a-database

# TODO: *autodetect the Primary in a RS instead of assuming the first mongod is the Primary. script may fail randomly until this is done.
# Poll instead of sleeping to make sure that each server is up
# Use dns registration to hide randomness of ports
# 	Print a list of servers with the DNS aliases
# ? fork processes into subshells and allow configurable detach option
# Test Arbiter code
use strict;
use Net::Address::IP::Local;
use List::Util qw(min any);

# Start configuration #
my $clusterName = 'c1'; # use a unique name for each cluster on one host
my $nConfigSvrs = 1; # 'mongoc'
my $nShards = 1; # Each shard is also a Replica Set
my $nReplicas = 3; # 'mongod's per Set; named rs#m#. For Shards without replication, set this to '1'
my $nQuerySvrs = 1; # 'mongos'

my %port = (
	mongoc => 27019,
	mongod => 27018,
	mongos => 27017
);

my $sleepTime = 4; # seconds; prevents the host from freezing

my $username = "employee";
my $password = "Sunshine4u";
my $role = "readWrite";
my $authenticationDatabase = "test";

# End configuration #
my $hostIP = Net::Address::IP::Local->public;
my $arbiter = !(min(7, $nReplicas) % 2); # Arbiter is necessary when there is an even number of voting members in a Replica Set; max of 7 voting members
my $baseImg = "${clusterName}mongodbimg";

cleanup();

# Set proper permissions on keyfile
terminal("chmod 600 provision/etc/mongokey");

# Build image of MongoDB on CentOS-6.5
terminal("docker build -t $baseImg .");

# Create config server containers
my $mongocIPString;
my @mongocList;
my $mongocString;
my $exposedPort;


my $authImg = createAuthImage('mongoc', $baseImg, $clusterName);
for my $c (1..$nConfigSvrs)
{
	my $containerName = "${clusterName}mongoc$c";
	
	launchServer('mongoc', $containerName, $authImg);
	sleep($sleepTime);
	
	$exposedPort = getExposedPort($containerName);
	$mongocIPString = $hostIP . ':' . $exposedPort;
	unshift @mongocList, $mongocIPString;
}
$mongocString = join(",", @mongocList);
$mongocString =~ s/\s+//g; # Remove line-break

# Create Replica Set member containers
for my $s (1..$nShards)
{
	# Assume the first mongod is the Primary member
	my $primary = "${clusterName}rs${s}m1";
	$authImg = createAuthImage('mongod', $baseImg, "${clusterName}primary$s"); # auth image for the primary
	for my $r (1..$nReplicas)
	{ # Create members
		my $containerName = "${clusterName}rs${s}m${r}";
		launchServer('mongod', $containerName, $authImg, "--replSet rs$s");
		sleep(2*$sleepTime);

		# Initialize the Replica Set
		terminal("docker exec $primary mongo --port $port{mongod} -u root -p root --authenticationDatabase admin --eval \"printjson(rs.initiate())\"") if ($r == 1);
		
		$authImg = createAuthImage('mongod', $baseImg, "${clusterName}secondary$s") if ($containerName eq $primary); # auth image for the secondaries
	}

	for my $r (1..$nReplicas)
	{ # Register members with the Primary
		my $containerName = "${clusterName}rs${s}m${r}";
		my $n = $r - 1;
		$exposedPort = getExposedPort($containerName);

		# Register Member with the Set
		terminal("docker exec $primary mongo --port $port{mongod} -u root -p root --authenticationDatabase admin  --eval \"printjson(rs.add(\'$hostIP:$exposedPort\'))\"") unless ($containerName eq $primary);
		# Configure Member
		terminal("docker exec $primary mongo --port $port{mongod} -u root -p root --authenticationDatabase admin  --eval \"
											printjson(cfg = rs.conf());
											cfg.members[$n].host = \'$hostIP:$exposedPort\';
											printjson(rs.reconfig(cfg));\""); # all in one eval so that 'cfg' is persistent
	}
	if ($arbiter)
	{
		my $containerName = "${clusterName}rs${s}a";
		launchServer('mongod', $containerName, $authImg, "--replSet rs$s");
		sleep(2*$sleepTime);
		# Register Arbiter
		terminal("docker exec $containerName mongo --port $port{mongod} -u root -p root --authenticationDatabase admin  --eval \"printjson(rs.addArb(\'$hostIP:$exposedPort\'))\"");
	}
}

# Create query router containers
for my $q (1..$nQuerySvrs)
{
	my $containerName = "${clusterName}mongos$q";
	launchServer('mongos', $containerName, $baseImg, "--configdb $mongocString");
	sleep($sleepTime);
}

sleep(2*$sleepTime);
# Generate mongo Shell eval string for registering Shards
my $regShardString;
for my $s (1..$nShards)
{
	$regShardString = $regShardString . "printjson(sh.addShard(\'rs$s/";
	for my $r (1..$nReplicas)
	{
		my $containerName = "${clusterName}rs${s}m${r}";
		$exposedPort = getExposedPort($containerName );
		$regShardString = $regShardString . "$hostIP:$exposedPort";
		$regShardString = $regShardString . ',' if ($r != $nReplicas);
	}
	$regShardString = $regShardString . "\'));";
}

# Register Shards with each mongos server
=not necessary
for my $q (1..$nQuerySvrs)
{
	my $containerName = "${clusterName}mongos$q";
	terminal("docker exec $containerName mongo --port $port{mongos} --eval \"$regShardString\"");
	sleep($sleepTime);
}
=cut
terminal("docker exec ${clusterName}mongos1 mongo --port $port{mongos} -u root -p root --authenticationDatabase admin  --eval \"$regShardString\"");
cleanup();

sub getPrimary()
{ # Returns the container name of the Primary; unused
	my $set = shift;
	my $repInfo = terminal("docker exec ${clusterName}rs${set}m1 mongo --port $port{mongod} -u root -p root --authenticationDatabase admin  --eval \"rs.printSlaveReplicationInfo()\"");
	$repInfo =~ /source: \d+\.\d+\.\d+\.\d+:(\d+).+source: \d+\.\d+\.\d+\.\d+:(\d+)/s;
	my @secondaries = [$1, $2];
	for my $r (1..$nReplicas)
	{
		if (!(any {getExposedPort("rs${set}m${r}") == $_} @secondaries))
		{
			return "rs${set}m${r}";
		}
	}
}

sub createAuthImage()
{
	my ($serverType, $prevImg, $prepend) = @_;
	my $exe = getExe($serverType);
	my $authImage = "$prepend${serverType}authimg";
	
	return $authImage if (`docker images` =~ /\Q^$authImage\s+/s);
	
	# Create image with a user
	terminal("docker run --name $authImage -i -d --net bridge --expose $port{$serverType} -P $prevImg $exe --port $port{$serverType} --config /etc/$serverType.conf");
	sleep(2*$sleepTime);
	createRootUser($authImage, $port{$serverType});
	terminal("docker exec $authImage mongo --port $port{$serverType} --eval \"db = db.getSiblingDB(\'admin\'); db.auth(\'root\', \'root\'); db.shutdownServer()\"");
	sleep($sleepTime);
	terminal("docker commit $authImage $authImage");
	return $authImage;
}

sub launchServer()
{
	my ($serverType, $containerName, $image, $additionalFlags) = @_;
	my $exe = getExe($serverType);
	terminal("docker run --name $containerName -i -d --net bridge --expose $port{$serverType} -P $image $exe --port $port{$serverType} --config /etc/$serverType.conf $additionalFlags");
}

sub getExe()
{
	my $serverType = shift;
	if ($serverType eq 'mongos') { return 'mongos' }
	else { return 'mongod' }
}

sub createRootUser()
{
	my $containerName = shift;
	my $port = shift;
	terminal("docker exec $containerName mongo --port $port --eval \"db = db.getSiblingDB(\'admin\'); db.createUser({user:\'root\', pwd:\'root\', roles:[\'root\']}); db.auth(\'root\', \'root\'); db = db.getSiblingDB(\'$authenticationDatabase\'); db.createUser({user:\'$username\', pwd:\'$password\', roles:[\'$role\']});\"");
}

sub getExposedPort() 
{
	my $containerName = shift;
	$exposedPort = `docker port $containerName | cut -d ":" -f2`;
	$exposedPort =~ s/\s+//g;
	return $exposedPort;
}

sub cleanup()
{
	# Remove old containers and images
	#terminal('docker stop $(docker ps -a -q)'); # Causes the VM to freeze; rewrite to pause between each container
	terminal('docker rm $(docker ps -a -q) &> /dev/null');
	terminal('docker images') =~ /$baseImg\s+.+?\s+([a-z0-9]{12})\s+/s;
	
	# Do not remove the base image
	my $baseId = $1;
	my @images = split "\n", terminal('docker images -a -q');
	@images = grep {!/$baseId/} @images;
	my $imageList = join(' ', @images);
	terminal("docker rmi @images &> /dev/null");
}

sub terminal()
{ # Print and run a command; print and return the output
	my $command = shift;
	my $output;
	$command =~ s/\t+//g;
	print("\n$command\n"); # Print the command
	$output = `$command`;
	print($output); # Run the command and print the output
	return $output;
}