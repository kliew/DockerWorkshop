# Creates a specified number of shard, query, and config servers for MongoDB 3.0.3
# Increase $sleepTime or free up CPU if the cluster fails to start properly
# Mongo servers can be accessed by $hostP:$exposedPort
# You must manually enable sharding for each collection and database
# 	docs.mongodb.org/manual/tutorial/deploy-shard-cluster/#enable-sharding-for-a-database

# TODO: *autodetect the Primary in a RS instead of assuming the first mongod is the Primary. script will fail randomly until this is done.
# Print a list of servers with the Primary indicated
# fork processes into subshells and allow configurable detach option
# use 'mongoimport'  to allow importing databases provisioned by Dockerfile
# 	after importing, get all db names and auto-enable sharding for each db and collection
# Test Arbiter code
use strict;
use Net::Address::IP::Local;
use List::Util qw(min);

# Start configuration #
my $clusterName = 'c1'; # use a unique name for each cluster on one host
my $nConfigSvrs = 1; # 'mongoc'
my $nShards = 2; # Each shard is also a Replica Set
my $nReplicas = 3; # 'mongod's per Set; named rs#m#
my $nQuerySvrs = 2; # 'mongos'

my $mongocPort = 27019;
my $mongodPort = 27018;
my $mongosPort = 27017;

my $sleepTime = 4; # seconds; prevents the host from freezing

my $username = "root";
my $password = "root";
my $authenticationDatabase = "admin";

# End configuration #
my $hostIP = Net::Address::IP::Local->public;
my $arbiter = !(min(7, $nReplicas) % 2); # Arbiter is necessary when there is an even number of voting members in a Replica Set; max of 7 voting members

# Remove old containers
#terminal('docker stop $(docker ps -a -q)'); # Causes the VM to freeze; rewrite to pause between each container
terminal('docker rm $(docker ps -a -q)');

# Build image of MongoDB on CentOS-6.5
terminal("docker build -t mongodbimg .");

# Create config server containers
my $mongocIPString;
my @mongocList;
my $mongocString;
my $exposedPort;
for my $c (1..$nConfigSvrs)
{
	my $containerName = "${clusterName}mongoc$c";
	terminal("docker run --name $containerName -i -d --net bridge --expose $mongocPort -P mongodbimg mongod --port $mongocPort --config /etc/mongoc.conf");
	sleep($sleepTime);
	$exposedPort = getExposedPort($containerName);
	$mongocIPString = $hostIP . ':' . $exposedPort;
	unshift @mongocList, $mongocIPString;
}
$mongocString = join(",", @mongocList);
$mongocString =~ s/\s+//g; # Remove line-break

# Create Replica Set member containers
for my $s (1..$nShards)
{
	# Assume the first mongod is the Primary member
	my $primary = "${clusterName}rs${s}m1";
	for my $r (1..$nReplicas)
	{ # Create members
		my $containerName = "${clusterName}rs${s}m${r}";
		terminal("docker run --name $containerName -i -d --net bridge --expose $mongodPort -P mongodbimg mongod --replSet rs$s --port $mongodPort --config /etc/mongod.conf --noauth");
		sleep(2*$sleepTime);

		# Initialize the Replica Set
		terminal("docker exec $primary mongo --port $mongodPort --eval \"printjson(rs.initiate())\"") if ($r == 1);
	}

	for my $r (1..$nReplicas)
	{ # Register members with the Primary
		my $containerName = "${clusterName}rs${s}m${r}";
		my $n = $r - 1;
		$exposedPort = getExposedPort($containerName);

		# Register Member with the Set
		terminal("docker exec $primary mongo --port $mongodPort --eval \"printjson(rs.add(\'$hostIP:$exposedPort\'))\"") unless ($containerName eq $primary);
		# Configure Member
		terminal("docker exec $primary mongo --port $mongodPort --eval \"
											printjson(cfg = rs.conf());
											cfg.members[$n].host = \'$hostIP:$exposedPort\';
											printjson(rs.reconfig(cfg));\""); # all in one eval so that 'cfg' is persistent
	}
	if ($arbiter)
	{
		my $containerName = "${clusterName}rs${s}a";
		terminal("docker run --name $containerName -i -d --net bridge --expose $mongodPort -P mongodbimg mongod --replSet rs$s --port $mongodPort --config /etc/mongoa.conf");
		sleep(2*$sleepTime);
		# Register Arbiter
		terminal("docker exec $containerName mongo --port $mongodPort --eval \"printjson(rs.addArb(\'$hostIP:$exposedPort\'))\"");
	}
}

# Create query router containers
for my $q (1..$nQuerySvrs)
{
	my $containerName = "${clusterName}mongos$q";
	terminal("docker run --name $containerName -i -d --net bridge --expose $mongosPort -P mongodbimg mongos --port $mongosPort --configdb $mongocString --config /etc/mongos.conf");
	sleep($sleepTime);
	$exposedPort = getExposedPort($containerName);
}

sleep(2*$sleepTime);
# Generate mongo Shell eval string for registering Shards
my $regShardString;
for my $s (1..$nShards)
{
	$regShardString = $regShardString . "printjson(sh.addShard(\'rs$s/";
	for my $r (1..$nReplicas)
	{
		my $containerName = "${clusterName}rs${s}m${r}";
		$exposedPort = getExposedPort($containerName );
		$regShardString = $regShardString . "$hostIP:$exposedPort";
		$regShardString = $regShardString . ',' if ($r != $nReplicas);
	}
	$regShardString = $regShardString . "\'));";
}

# Register Shards with each mongos server
=not necessary
for my $q (1..$nQuerySvrs)
{
	my $containerName = "${clusterName}mongos$q";
	terminal("docker exec $containerName mongo --port $mongosPort --eval \"$regShardString\"");
	sleep($sleepTime);
}
=cut
terminal("docker exec ${clusterName}mongos1 mongo --port $mongosPort --eval \"$regShardString\"");
sleep($sleepTime);

#TODO: get a list of DB names to auto-enable sharding for each DB and Collection

sub getExposedPort() 
{
	my $containerName = shift;
	$exposedPort = `docker port $containerName | cut -d ":" -f2`;
	$exposedPort =~ s/\s+//g;
	return $exposedPort;
}

sub terminal()
{
	my $command = shift;
	my $output;
	$command =~ s/\t+//g;
	print("\n$command\n"); # Print the command
	$output = `$command`;
	print($output); # Run the command and print the output
	return $output;
}





