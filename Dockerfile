# Create an image of MongoDB on CentOS-6.6 to run Mongo mongod, mongos, and config servers

FROM centos:6.6

MAINTAINER Kevin Liew <kevinl@simba.com>

USER root

WORKDIR /tmp

ENV MONGO_VERSION 3.0.5

ENV MONGO_HOME /usr/local/mongodb
ENV DBPATH /data/db
ENV CFGPATH /data/configdb

ENV PATH $MONGO_HOME/mongodb-linux-x86_64-$MONGO_VERSION/bin:$PATH

# Provision the MongoDB tar file (archives are automatically extracted by Docker, so don't run tar on the image)
ADD mongodb-linux-x86_64-$MONGO_VERSION.tgz /tmp/

# Provision configuration files
ADD provision/etc /etc

# Provision database
#ADD provision/data/db /data/db

# Install MongoDB for 64-bit Linux
RUN mkdir -p $MONGO_HOME && \
    cp -R -n mongodb-linux-x86_64-$MONGO_VERSION/ $MONGO_HOME && \
    mkdir -p $DBPATH && \
    mkdir -p $CFGPATH